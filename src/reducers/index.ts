import { EnthusiasmAction } from '../actions';
import { StoreState } from '../types';
import { INCREMENT_ENTHUSIASM, DECREMENT_ENTHUSIASM } from '../constants';
import { handleActions } from "redux-actions";

// export function enthusiasm(state: StoreState, action: EnthusiasmAction): StoreState {
//     switch (action.type) {
//         case INCREMENT_ENTHUSIASM:
//             return { ...state, enthusiasmLevel: state.enthusiasmLevel + 1 };
//         case DECREMENT_ENTHUSIASM:
//             return { ...state, enthusiasmLevel: Math.max(1, state.enthusiasmLevel - 1) };
//         default:
//             return state;
//     }
// }

const initialState: StoreState = {
    enthusiasmLevel: 1,
    languageName: "TypeScript",
};

export const enthusiasm = handleActions<StoreState, EnthusiasmAction>(
    {
        [INCREMENT_ENTHUSIASM]: (state: StoreState) => {
            return { ...state, enthusiasmLevel: state.enthusiasmLevel + 1 };
        },
        [DECREMENT_ENTHUSIASM]: (state: StoreState) => {
            return { ...state, enthusiasmLevel: state.enthusiasmLevel - 1 };
        }
    }, 
    initialState
);